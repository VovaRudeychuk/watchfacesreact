var React = require('react');
import * as firebase from 'firebase';


var adminDashboard = React.createClass({

    componentDidMount: function () {
        this.usersRef = firebase.database().ref().child('users');
        this.usersRef.on('value', function (snapshot) {
            var usersData = [];
            var watch = snapshot.numChildren();
            usersData.push(watch);
            this.setState({
                usersData: usersData
            });
        }.bind(this));

        this.watchesRef = firebase.database().ref().child('watches');
        this.watchesRef.on('value', function (snapshot) {
            var watchesData = [];
            var watch = snapshot.numChildren();
            watchesData.push(watch);
            this.setState({
                watchesData: watchesData
            });
        }.bind(this));

        this.postRef = firebase.database().ref().child('posts');
        this.postRef.on('value', function (snapshot) {
            var postData = [];
            var watch = snapshot.numChildren();
            postData.push(watch);
            this.setState({
                postData: postData
            });
        }.bind(this));


        this.bannersRef = firebase.database().ref().child('banners');
        this.bannersRef.on('value', function (snapshot) {
            var bannersData = [];
            var watch = snapshot.numChildren();
            bannersData.push(watch);
            this.setState({
                bannersData: bannersData
            });
        }.bind(this));
    },

    getInitialState: function() {

        return {
            loggedIn: (null !== firebase.auth().currentUser),
            usersData: [],
            watchesData: [],
            postData: [],
            bannersData: []

        };
    },

    render: function(){

        if (this.state.loggedIn) {
            return (<section >
                <h1>Admin Panel Boost Solutions Watch Faces </h1>
                <table className="table table-bordered table-striped">
                    <tbody>
                    <tr>
                        <td>
                            <span className="glyphicon glyphicon-user" aria-hidden="true"></span>
                            <b>Total Registered Users</b>

                        </td>
                        <td>
                            <span className="glyphicon glyphicon-time" aria-hidden="true"></span>

                            <b>Total WatchFaces</b>

                        </td>
                        <td>
                            <span className="glyphicon glyphicon-file" aria-hidden="true"></span>
                            <b>Available Posts</b>

                        </td>
                        <td>

                            <span className="glyphicon glyphicon-picture" aria-hidden="true"></span>
                            <b>Total Banners </b></td>

                    </tr>
                    <tr>


                        <td><b>{this.state.usersData}</b></td>
                        <td><b>{this.state.watchesData}</b></td>
                        <td><b>{this.state.postData}</b></td>
                        <td><b>{this.state.bannersData}</b></td>


                    </tr>

                    </tbody>
                </table>

            </section>)


        }else {

            console.log('You are not login');
        }
    }
});

module.exports = adminDashboard;