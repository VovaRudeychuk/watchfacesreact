var React = require('react');

var WatchFaces = React.createClass({
    getInitialState: function() {
        return {
            loggedIn: (null !== firebase.auth().currentUser)
        }
    },
    render: function(){
        if (this.state.loggedIn) {
            return (
                <section>

                    <h1>WatchFaces List </h1>
                    <form className="form-inline">
                        <a href="#/watches-edit">
                            <button className="btn btn-primary">Add new WatchFace </button>
                        </a>
                    </form>


                    <form>
                        <div className="form-group">
                            <div className="input-group">
                                <div className="input-group-addon"><i className="fa fa-search"></i></div>
                                <input type="text" className="form-control" placeholder="Search in List " ></input>
                            </div>
                        </div>
                    </form>


                    <table className="table table-bordered table-striped">
                        <thead>
                        <tr>

                            <th>
                                <span className="glyphicon glyphicon-time" aria-hidden="true"></span>
                                <a href="#">
                                    <b>WatchFace name</b>
                                    <span className="fa fa-caret-down"></span>
                                    <span className="fa fa-caret-up"></span>
                                </a>
                            </th>
                            <th><span className="glyphicon glyphicon-picture" aria-hidden="true"></span>

                                <b>Icon circle </b>


                            </th>

                            <th><span className="glyphicon glyphicon-picture" aria-hidden="true"></span>

                                <b>Icon square </b>

                            </th>
                            <th><span className="glyphicon glyphicon-tags" aria-hidden="true"></span>
                                <a href="#/watches">
                                    <b>Distribution</b>
                                    <span  className="fa fa-caret-down"></span>
                                    <span  className="fa fa-caret-up"></span>
                                </a>

                            </th>
                            <th><span className="glyphicon glyphicon-usd" aria-hidden="true"></span>
                                <a href="#/watches">
                                    <b>Price</b>
                                    <span className="fa fa-caret-down "></span>
                                    <span className="fa fa-caret-up "></span>
                                </a>

                            </th>

                            <th><span className="glyphicon glyphicon glyphicon-phone" aria-hidden="true"></span>

                                <b>Interactive</b>

                            </th>


                            <th><span className="glyphicon glyphicon-info-sign" aria-hidden="true"></span>

                                <b>Package Name </b>

                            </th>
                            <th><span className="glyphicon glyphicon-edit" aria-hidden="true"></span>

                                <b>Last Updated at </b>

                            </th>

                            <th><span className="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                                <b>Actions</b></th>
                        </tr>






                        </thead>

                        <tbody>
                        <tr>



                            <td className="col-md-1 text-center"><p>Weather</p></td>
                            <td className="col-md-1 text-center">

                                <a>

                                    <img className="img-thumbnail" width="100" src="https://s3-eu-west-1.amazonaws.com/boosttestwatchface/com.boost.weather.ui.WeatherWatchFace/circleIcon/mock_up_circle_weather.png"></img>
                                </a>

                            </td>

                            <td className="col-md-1 text-center">
                                <a>
                                    <img className="img-thumbnail" width="100" src="https://s3-eu-west-1.amazonaws.com/boosttestwatchface/com.boost.weather.ui.WeatherWatchFace/squareIcon/mock_up_square_weather.png"></img>
                                </a>
                            </td>
                        <td className="col-md-1 text-center "><p>Paid</p></td>
                            <td className="col-md-1 text-center"><p>0.99</p></td>
                            <td className="col-md-1 text-center"><p>NO</p></td>
                            <td className="col-md-1 text-center"><p>com.boost.weather</p></td>
                            <td className="col-md-1 text-center"><p>2016-06-03  16:21:12 </p></td>



                            <td className="col-md-1 text-center">

                                <a href="#/watches-edit/-KFcXzxVnOR9OB8lzPHu">
                                    <button className="btn btn-info btn-xs">
                                        <span className="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                                    </button>
                                </a>

                                <button className="btn btn-danger btn-xs">
                                    <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>




                            </td>
                        </tr><tr>



                            <td className="col-md-1 text-center"><p>Vinyl</p></td>
                            <td className="col-md-1 text-center">

                                <a>

                                    <img className="img-thumbnail" width="100" src="https://d1pqyjtezxhbh0.cloudfront.net/com.boost.vinyl.ui.VinylWatchFace/circleIcon/ic_vinyl2_circle.png"></img>
                                </a>

                            </td>

                            <td className="col-md-1 text-center">
                                <a>
                                    <img className="img-thumbnail" width="100" src="https://d1pqyjtezxhbh0.cloudfront.net/com.boost.vinyl.ui.VinylWatchFace/squareIcon/ic_vinyl_square.png"></img>
                                </a>
                            </td>

                            <td className="col-md-1 text-center"><p>Paid</p></td>
                            <td className="col-md-1 text-center"><p>0.99</p></td>
                            <td className="col-md-1 text-center"><p>NO</p></td>
                            <td className="col-md-1 text-center"><p>com.boost.vinyl</p></td>
                            <td className="col-md-1 text-center"><p>2016-03-26  18:02:02 </p></td>



                            <td className="col-md-1 text-center">

                                <a href="#/watches-edit/Vinyl">
                                    <button className="btn btn-info btn-xs">
                                        <span className="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                                    </button>
                                </a>

                                <button className="btn btn-danger btn-xs">
                                    <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                </button>




                            </td>
                        </tr>

                        </tbody></table>


                </section>
            )
        }else {

            console.log('You are not login');
        }
    }
});

module.exports = WatchFaces;