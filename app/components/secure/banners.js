var React = require('react');

var Banners = React.createClass({
    componentWillMount: function () {
        this.bannersRef = firebase.database().ref().child('banners');
        this.bannersRef.on('value', function (snapshot) {
            var bannersPosts = [];
            var banners = snapshot.val();

            bannersPosts.push(banners);
            console.log(bannersPosts);
            this.setState({
                bannersPosts: bannersPosts
            });
        }.bind(this));
    },

    getInitialState: function() {
        return {
            loggedIn: (null !== firebase.auth().currentUser),
            bannersPosts: []


        }

    },

    render: function(){
        console.log(this.state.bannersPosts[0]);
        if (this.state.loggedIn) {
            return (<section>

                <h1>Banners</h1>
                <p>{this.state.bannersPosts.imageUrl}</p>
                <form className="form-inline">
                    <a href="#/banners-edit">
                        <button className="btn btn-primary">Add Banner</button>
                    </a>
                </form>

                <form>
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-addon"><i className="fa fa-search"></i></div>
                            <input type="text" className="form-control" placeholder="Search in banner"></input>
                        </div>
                    </div>
                </form>


                <table className="table table-bordered table-striped">
                    <thead>
                    <tr>

                        <th><span className="glyphicon glyphicon-picture" aria-hidden="true"></span>
                            Banner image</th>
                        <th><span className="glyphicon glyphicon-tags" aria-hidden="true"></span>
                            <a href="/banners">
                                <b>Package Name</b>
                                <span className="fa fa-caret-down"></span>
                                <span className="fa fa-caret-up"></span>
                            </a>
                        </th>
                        <th><span className="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                            <b>Actions</b></th>
                    </tr>


                    </thead>
                    <tbody>
                    <tr>

                        <td className="col-md-1 text-center">
                            <a>
                                <img src="" className="img-thumbnail" width="150"></img>
                            </a>
                        </td>
                        <td className="col-md-1 text-center">com.boost.testBanner</td>
                        <td className="col-md-1 text-center">

                            <a href="#/banners-edit/-KBw73nYZhyJfbtnHqWt">
                                <button className="btn btn-info btn-xs">
                                    <span className="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                                </button>
                            </a>

                            <button className="btn btn-danger btn-xs">
                                <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
                            </button>




                        </td>


                    </tr><tr>

                        <td className="col-md-1 text-center">
                            <a>
                                <img className="img-thumbnail" width="150" src="https://d1pqyjtezxhbh0.cloudfront.net/banners/test_banner.png"></img>
                            </a>
                        </td>
                        <td className="col-md-1 text-center ">google</td>
                        <td className="col-md-1 text-center">

                            <a href="#/banners-edit/sdfsgdf">
                                <button className="btn btn-info btn-xs">
                                    <span className="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                                </button>
                            </a>

                            <button className="btn btn-danger btn-xs">
                                <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
                            </button>




                        </td>

                    </tr>
                    </tbody><tbody>
                </tbody></table>

                <div className="text-center">

                </div>



            </section>)


        }else {

            console.log(404);
        }
    }
});

module.exports = Banners;