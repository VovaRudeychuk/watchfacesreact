var React = require('react');

var Posts = React.createClass({
    getInitialState: function() {
        return {
            loggedIn: (null !== firebase.auth().currentUser)
        }
    },
    render: function(){
        if (this.state.loggedIn) {
            return(
                <section>
                <h1>Coming soon</h1>
                <form className="form-inline">
                    <a href="#/posts-edit">
                        <button className="btn btn-primary">Add Post</button>
                    </a>
                </form>

                 <form>
                     <div className="form-group">
                         <div className="input-group">
                             <div className="input-group-addon"><i className="fa fa-search"></i></div>
                             <input type="text" className="form-control" placeholder="Search in posts"></input>
                         </div>
                     </div>
                 </form>


                <table className="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th><span className="glyphicon glyphicon-picture" aria-hidden="true"></span>
                            <b>Post Image</b>
                        </th>
                        <th><span className="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            <b>Post Title</b>
                        </th>
                        <th><span className="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            <b>Post Text</b>
                        </th>
                    </tr>
                    </thead>
                </table>
                </section>
            )
        }else {

            console.log('You are not login');
        }
    }
});

module.exports = Posts;