var React = require('react');

var PushMessages = React.createClass({
    getInitialState: function() {
        return {
            loggedIn: (null !== firebase.auth().currentUser)
        }
    },
    render: function(){
        if (this.state.loggedIn) {
            return (<section>

                <h1>WatchFaces List </h1>

                <form className="form-inline mb_20">
                    <a href="#/messages-edit">
                        <button className="btn btn-primary">Add new Push Message </button>
                    </a>
                </form>


                <form>
                    <div className="form-group">
                        <div className="input-group">
                            <div className="input-group-addon"><i className="fa fa-search"></i></div>
                            <input type="text" className="form-control" placeholder="Search in List "></input>
                        </div>
                    </div>
                </form>


                <table className="table table-bordered table-striped">
                    <thead>
                    <tr>

                        <th>
                            <span className="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                            <a href="#/messages">
                                <b>Message title</b>
                                <span className="fa fa-caret-down"></span>
                                <span className="fa fa-caret-up"></span>
                            </a>
                        </th>
                        <th>
                            <span className="glyphicon glyphicon-tag" aria-hidden="true"></span>
                            <a href="#/messages">
                                <b>Message type</b>
                                <span  className="fa fa-caret-down"></span>
                                <span  className="fa fa-caret-up"></span>
                            </a>
                        </th>


                        <th><span className="glyphicon glyphicon-pencil" aria-hidden="true"></span>

                            <b>Message body </b>


                        </th>

                        <th><span className="glyphicon glyphicon-picture" aria-hidden="true"></span>

                            <b>Image </b>

                        </th>



                        <th><span className="glyphicon glyphicon-link" aria-hidden="true"></span>
                            <b>Actions</b></th>



                        <th><span className="glyphicon glyphicon-send" aria-hidden="true"></span>
                            <b>Send Push</b></th>


                    </tr>


                    </thead>

                    <tbody>
                    <tr>



                        <td className="col-md-1 text-center"><p>Watch Face: Magic 8 Ball released</p></td>
                        <td className="col-md-1 text-center"><p>preview</p></td>

                        <td className="col-md-1 text-center"><p>Watch preview and get it!</p></td>

                        <td className="col-md-1 text-center">

                            <a>

                                <img className="img-thumbnail" width="100" src="https://d1pqyjtezxhbh0.cloudfront.net/pushMessagess/aoIc2JUT5pp6nfa_img_icon_mb.png"></img>
                            </a>

                        </td>



                        <td className="col-md-1 text-center">

                            <a href="#/messages-edit/-KGJHXwU9z_hJAq6OgS7">
                                <button className="btn btn-info btn-xs">
                                    <span className="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                                </button>
                            </a>

                            <button className="btn btn-danger btn-xs" >
                                <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
                            </button>




                        </td>
                        <td className="col-md-1 text-center">

                            <button className="btn btn-success btn-xs">
                                <span className="glyphicon glyphicon-send" aria-hidden="true"></span>
                            </button>




                        </td>

                    </tr>
                    <tr>



                        <td className="col-md-1 text-center"><p>Watch Face: Comics released</p></td>
                        <td className="col-md-1 text-center"><p>play</p></td>

                        <td className="col-md-1 text-center"><p>Get it now for free!</p></td>

                        <td className="col-md-1 text-center">

                            <a>

                                <img className="img-thumbnail" width="100" src="https://d1pqyjtezxhbh0.cloudfront.net/pushMessagess/AJdiqHt5GX1lsLO_ic_launcher_HQ.png"></img>
                            </a>

                        </td>



                        <td className="col-md-1 text-center">

                            <a href="#/messages-edit/-KFybDiLwGp2FIHTHmAG">
                                <button className="btn btn-info btn-xs">
                                    <span className="glyphicon glyphicon-wrench" aria-hidden="true"></span>
                                </button>
                            </a>

                            <button className="btn btn-danger btn-xs">
                                <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
                            </button>




                        </td>
                        <td className="col-md-1 text-center">

                            <button className="btn btn-success btn-xs">
                                <span className="glyphicon glyphicon-send" aria-hidden="true"></span>
                            </button>




                        </td>

                    </tr>

                    </tbody></table>


            </section>)
        }else {

            console.log('You are not login');
        }
    }
});

module.exports = PushMessages;