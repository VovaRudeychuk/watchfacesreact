var React = require('react');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Link = ReactRouter.Link;
import * as firebase from 'firebase';

var Main = React.createClass({
    getInitialState: function() {
        return {
            loggedIn: (null !== firebase.auth().currentUser)
        }
    },
    componentWillMount: function() {
        firebase.auth().onAuthStateChanged(firebaseUser => {

            this.setState({
                loggedIn: (null !== firebaseUser)
            })

            if (firebaseUser) {
                console.log("Logged IN", firebaseUser);
            } else {
                console.log('Not logged in');
            }
        });
    },
    render: function() {
        var loginOrOut;
        var register;
        var watchFaces;
        var banners;
        var posts;
        var pushmessages;
        var appUser;
        if (this.state.loggedIn) {
            loginOrOut = <li>
                <Link to="/logout" className="navbar-brand">Logout</Link>
            </li>;
            pushmessages = <li>
                <Link to="/pushmessages" className="navbar-brand">
                    Push Messages
                </Link>
            </li>;
            watchFaces =  <li>
                <Link to="/watchfaces" className="navbar-brand">
                    Watch Faces
                </Link>
            </li>;
            banners = <li>
                <Link to="/banners" className="navbar-brand">
                    Banners
                </Link>
            </li>;
            posts = <li>
                <Link to="/posts" className="navbar-brand">
                    Posts
                </Link>
            </li>;
            appUser = <li>
                <Link to="/appUser" className="navbar-brand">
                    App User
                </Link>
            </li>;

            register = null
        } else {
            loginOrOut = <li>
                <Link to="/login" className="navbar-brand">Login</Link>
            </li>;


        }
        return (
            <span>
                <nav className="navbar navbar-default navbar-static-top">
                    <div className="container">
                        <div className="navbar-header">
                            <Link to="/adminDashboard" className="navbar-brand">
                                WatchFaces APanel
                            </Link>

                        </div>

                         <ul className="nav navbar-nav pull-left">

                             {banners}
                             {posts}
                             {watchFaces}
                             {register}
                             {pushmessages}
                             {appUser}
                         </ul>
                        <ul className="nav navbar-nav pull-right">

                            {loginOrOut}

                        </ul>
                    </div>
                </nav>
                <div className="container">
                    <div className="row">
                        {this.props.children}
                    </div>
                </div>
            </span>
        )
    }
});

module.exports = Main;
