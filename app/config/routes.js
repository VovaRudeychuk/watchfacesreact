var React = require('react');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var hashHistory = ReactRouter.hashHistory;
var IndexRoute = ReactRouter.IndexRoute;
var Main = require('../components/Main');
var Register = require('../components/login-register/Register');
var Login = require("../components/login-register/Login");
var Logout = require('../components/login-register/Logout');
var Posts = require('../components/secure/posts');
var WatchFaces = require('../components/secure/wathcFaces');
var PushMessages = require('../components/secure/pushmsg');
var Banners = require('../components/secure/banners');
var adminDashboard = require('../components/secure/adminDashboard');
var Home = require("../components/Home");
var AppUser = require('../components/secure/appUser')
var requireAuth = require('../utils/authenticated');


var routes = (
  <Router history={hashHistory}>
    <Route path='/' component={Main} >
      <IndexRoute component={Home}  />
      <Route path="login" component={Login} />
      <Route path="logout" component={Logout} />
      <Route path="register" component={Register} />
      <Route path="Posts" component={Posts}/>
      <Route path="Banners" component={Banners}/>
      <Route path="WatchFaces" component={WatchFaces}/>
      <Route path="PushMessages" component={PushMessages}/>
      <Route path="AppUser" component={AppUser}/>
      <Route path="adminDashboard" component={adminDashboard} onEnter={requireAuth}/>
    </Route>
  </Router>
);

module.exports = routes;
